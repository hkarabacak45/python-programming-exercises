#Define a function that can receive two integral numbers in string form and compute their sum and then print it in console.

def sum(string1, string2):
    num1 = int(string1)
    num2 = int(string2)
    return(num1 + num2)

print(sum("15", "14"))
