#Write a program that accepts a sequence of whitespace separated words as input and prints the words after removing all duplicate words and sorting them alphanumerically.

string = input("Enter a sequence of words separated by whitespace: ")

words = string.split(" ")
words = set(words)
words = list(words)
words = sorted(words)
print(" ".join(words))
