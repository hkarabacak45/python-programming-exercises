#You are required to write a program to sort the (name, age, height) tuples by ascending order where name is string, age and height are numbers. The tuples are input by console. The sort criteria is:
#1: Sort based on name;
#2: Then sort based on age;
#3: Then sort by score.
#The priority is that name > age > score.

people = []
while(True):
    person = input("Enter the name, age, height of a person or nothing to exit: ")
    if person == "":
        break
    people.append(person.split(","))

people.sort()
print(people)
