#Please write a program which count and print the numbers of each character in a string input by console.

letterCount = {}
string = input("Please enter a string: ")

for c in string:
    if c not in letterCount:
        letterCount[c] = 1
    else:
        letterCount[c] += 1

print("\n".join(["{} {}".format(keys, values) for keys, values in letterCount.items()]))
