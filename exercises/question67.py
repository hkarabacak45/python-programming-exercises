#Please write a program using generator to print the numbers which can be divisible by 5 and 7 between 0 and n in comma separated form while n is input by console.

number = int(input("PLease enter a number: "))

def generator(n):
    for i in range(n + 1):
        if i % 35 == 0:
            yield(i)

numbers = []
for i in generator(number):
    numbers.append(i)

print(numbers)
