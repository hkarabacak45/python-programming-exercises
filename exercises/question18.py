#A website requires the users to input username and password to register. Write a program to check the validity of password input by users.

import re

print("Press Control-C to exit the program at anytime.")
while(True):
    test = 0    #test fail
    password = input("Please check a password: ")
    if (len(password) < 6) | (len(password) > 12):
        pass
    elif not re.search("[a-z]", password):
        pass
    elif not re.search("[0-9]", password):
        pass
    elif not re.search("[A-Z]", password):
        pass
    elif not re.search("[$#@]", password):
        pass
    else:
        print("\"{}\" is secure".format(password))
        test = 1    #test passed
    if test == 0:
        print("\"{}\" is insecure".format(password))
