#By using list comprehension, please write a program to print the list after removing delete numbers which are divisible by 5 and 7 in [12,24,35,70,88,120,155].

given = [12,24,35,70,88,120,155]

new = [i for i in given if i % 35 != 0]
print(new)
