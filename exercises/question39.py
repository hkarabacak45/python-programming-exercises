#Define a function which can generate and print a tuple where the value are square of numbers between 1 and 20 (both included).

import question38

print(tuple(question38.partSquares(17)))
