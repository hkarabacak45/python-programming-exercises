#Define a function which can generate a dictionary where the keys are numbers between 1 and 20 (both included) and the values are square of keys. The function should just print the keys only.

def squares(n):
    squares = dict()
    for i in range(1, n):
        squares[i] = i**2
    for (keys, values) in squares.items():
        print(keys)

squares(17)
