#Write a program to generate and print another tuple whose values are even numbers in the given tuple (1,2,3,4,5,6,7,8,9,10).

given = (1,2,3,4,5,6,7,8,9,10)

given = list(given)

for i in given:
    if i % 2 == 1:
        given.remove(i)

print(tuple(given))
