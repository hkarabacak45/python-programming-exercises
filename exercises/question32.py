#Define a function which can print a dictionary where the keys are numbers between 1 and 3 (both included) and the values are square of keys.

def squares(n):
    squares = dict()
    for i in range(1, n):
        squares[i] = i**2
    print(squares)

squares(14)
