#Define a class named American and its subclass NewYorker.

class american():
    @staticmethod
    def printNationality():
        print("American")

class newYorker(american):
    @staticmethod
    def printRegionality():
        print("New Yorker")

newYorker = newYorker()
newYorker.printNationality()
newYorker.printRegionality()
