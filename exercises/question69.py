#Please write a program which accepts basic mathematic expression from console and print the evaluation result.

expression = list(input("Please enter a basic mathematic expression: "))

index = 1

for i in range(0, len(expression)):
    try:
        int(expression[i])
    except ValueError:
        index = i

if expression[index] == "+":
    print(int("".join(expression[:index])) + int("".join(expression[(index + 1):])))
elif expression[index] == "-":
    print(int("".join(expression[:index])) - int("".join(expression[(index + 1):])))
elif expression[index] == "*":
    print(int("".join(expression[:index])) * int("".join(expression[(index + 1):])))
elif expression[index] == "/":
    print(int("".join(expression[:index])) / int("".join(expression[(index + 1):])))
else:
    print("Error.")

#print(eval(expression)
