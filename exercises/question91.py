#Define a class Person and its two child classes: Male and Female. All classes have a method "getGender" which can print "Male" for Male class and "Female" for Female class.

class person():
    def getGender(self):
        return(None)

class male(person):
    def getGender(self):
        return("Male")

class female(person):
    def getGender(self):
        return("Female")

male = male()
female = female()
print(male.getGender())
print(female.getGender())
person = person()
print(person.getGender())
