#Write a program to compute 1/2+2/3+3/4+...+n/n+1 with a given n input by console (n>0).

number = int(input("Please enter a number: "))
sum = float(0)

for i in range(1, (number + 1)):
    sum += float(i / (i + 1))

print(sum)
