#Write a program to solve a classic ancient Chinese puzzle:
#We count 35 heads and 94 legs among the chickens and rabbits in a farm. How many rabbits and how many chickens do we have?

heads = 35
legs = 94

def solver(nHeads, nLegs):
    for i in range(nHeads+1):
        j = nHeads - i
        if 2*i + 4*j == nLegs:
            return("{} chickens and {} rabbits".format(i, j))
    return("No solutions.")

print(solver(heads, legs))
